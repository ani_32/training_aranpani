import 'package:flutter/material.dart';
import 'package:uitaskno1/connector/projects/project_connector.dart';

import '../../../views/Projects/Details.dart';
import 'package:readmore/readmore.dart';

List<String?> urls = [];
String status = '';

class ActiveTemples extends StatelessWidget {
  const ActiveTemples({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProjectConnector(
        builder: (BuildContext context, ProjectViewModel projectViewModel) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView.builder(
            itemCount: projectViewModel.projects.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              status = projectViewModel.projects[index].status ?? '';
              urls.add(projectViewModel
                  .projects[index].projectAttachment?[0].image_url);

              if (status == 'active') {
                return Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.04),
                        spreadRadius: 2,
                        blurRadius: 5,
                        offset: const Offset(0, 2),
                      ),
                    ],
                  ),
                  height: MediaQuery.of(context).size.height * 0.45,
                  width: MediaQuery.of(context).size.width * 0.45,
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Card(
                    elevation: 25,
                    shadowColor: Colors.black26,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Stack(
                          children: [
                            FractionallySizedBox(
                              widthFactor: 1,
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.network(
                                    urls[index]!,
                                    fit: BoxFit.cover,
                                    height: 220,
                                  )),
                            ),
                            Positioned(
                                top: 170,
                                left: -10,
                                child: Container(
                                  alignment: Alignment.topCenter,
                                  width: MediaQuery.of(context).size.width * 0.6,
                                  height:
                                      MediaQuery.of(context).size.height * 0.03,
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.white),
                                      borderRadius: BorderRadius.circular(20),
                                      color: Colors.white.withOpacity(0.6)),
                                  // child: Text('${projectViewModel.projects[index].start_date?.substring(1,6)} - ${projectViewModel.projects[index].end_date?.substring(1,6)}'),
                                  child: const Padding(
                                    padding: EdgeInsets.only(left: 25, top: 4),
                                    child: Text(
                                      '28 Jan, 2021 - 03 Feb, 2021',
                                      style:
                                          TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                )),
                            Positioned(
                                top: 10,
                                right: 10,
                                child: Container(
                                  width: 30,
                                  height: 30,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white.withOpacity(0.8),
                                  ),
                                  child: const Icon(
                                    Icons.notifications_active,
                                    color: Colors.black,
                                    size: 15,
                                  ),
                                ))
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Details(
                                                image_url: urls[index].toString(),
                                                name: projectViewModel
                                                    .projects[index].name
                                                    .toString(),
                                                end_date: projectViewModel
                                                    .projects[index].end_date
                                                    .toString(),
                                                start_date: projectViewModel
                                                    .projects[index].start_date
                                                    .toString(),
                                                estimated_amount: projectViewModel
                                                        .projects[index]
                                                        .estimated_amount ??
                                                    0,
                                                expensed_amount: projectViewModel
                                                        .projects[index]
                                                        .expensed_amount ??
                                                    0,
                                                updated_at: projectViewModel
                                                    .projects[index].updated_at
                                                    .toString(),
                                                completion: projectViewModel
                                                    .projects[index].completion!,
                                                incharge_mobile_number:
                                                    projectViewModel
                                                        .projects[index]
                                                        .incharge_mobile_number
                                                        .toString(),
                                                incharge_name: projectViewModel
                                                    .projects[index].incharge_name
                                                    .toString(),
                                                status: 'active',
                                                reg_number: projectViewModel
                                                    .projects[index].reg_number
                                                    .toString(),
                                              )));
                                },
                                child: Text(
                                  projectViewModel.projects[index].name ?? '',
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      color: Colors.black),
                                ),
                              ),
                              // const SizedBox(
                              //   height: 5,
                              // ),
                              const SizedBox(
                                height: 1,
                              ),
                              const Text(
                                '  Thirumanancheri,Chennai (X4X4+W6)',
                                style: TextStyle(fontWeight: FontWeight.w200),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              Row(
                                children: [
                                  const SizedBox(width: 5,),
                                  const Icon(
                                    Icons.call,
                                    color: Colors.blue,
                                  ),
                                  const SizedBox(width: 5,),
                                  Text(
                                    ' ${projectViewModel.projects[index].incharge_name}',
                                    style: const TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    '   ${projectViewModel.projects[index].incharge_mobile_number}',
                                    style: const TextStyle(color: Colors.blue),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              } else {
                return const SizedBox();
              }
            }, ),
      );
    });
  }
}
