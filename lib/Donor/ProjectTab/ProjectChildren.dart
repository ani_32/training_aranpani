import 'package:flutter/material.dart';
import 'package:uitaskno1/Donor/ProjectTab/projbody.dart';

class ProjectChildren extends StatelessWidget {
  const ProjectChildren({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [Container(
        height: 150,
        width: MediaQuery.of(context).size.width,
        color: Colors.deepOrange,
        child: Column(
          children: [
            const Text("Project Details",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white,fontSize: 30),),
            const SizedBox(height: 40,),
            Container(height:50,width: 200,color: Colors.orange,child:const ProjBody(),)

          ],
        ),
      ),
     ]
    );
  }
}
