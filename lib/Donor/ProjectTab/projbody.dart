import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:uitaskno1/Donor/ProjectTab/ActivePage/activetemples.dart';
import 'package:uitaskno1/Donor/ProjectTab/ActivePage/completedtemples.dart';
import 'package:uitaskno1/Donor/ProjectTab/ActivePage/proposedtemples.dart';
import 'package:uitaskno1/connector/projects/project_connector.dart';

// ........get call for this page...........

class ProjBody extends StatelessWidget {
  const ProjBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProjectConnector(
        builder: (BuildContext context, ProjectViewModel projectViewModel) {
      return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            foregroundColor: Colors.white,
            elevation: 0,
            flexibleSpace: Container(
              height: MediaQuery.of(context).size.height * 0.9,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Colors.orange, Colors.deepOrange],
                ),
              ),
            ),
            automaticallyImplyLeading: false,
            title: const Padding(
              padding: EdgeInsets.only(top: 15, left: 15),
              child: Text(
                "Projects",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
              ),
            ),
            centerTitle: false,
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(60),
              child: Container(
                alignment: Alignment.topCenter,
                width: MediaQuery.of(context).size.width * 0.9,
                height: MediaQuery.of(context).size.height * 0.06,
                decoration: BoxDecoration(
                    color: Colors.white24,
                    borderRadius: BorderRadius.circular(20)),
                child: TabBar(
                  padding: const EdgeInsets.all(8),
                  unselectedLabelColor: Colors.white,
                  labelColor: Colors.deepOrange,
                  indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.white),
                  tabs: const [
                    Tab(text: 'Active'),
                    Tab(
                      text: 'Planned',
                    ),
                    Tab(
                      text: 'Completed',
                    )
                  ],
                ),
              ),
            ),
          ),
          body: const TabBarView(
            children: [
              ActiveTemples(),
              ProposedTemples(),
              CompletedTemples(),
            ],
          ),
        ),
      );
    });
  }
}
