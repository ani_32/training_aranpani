import 'package:flutter/material.dart';
import 'package:uitaskno1/Donor/HomeTab/home_children.dart';
import 'package:uitaskno1/Donor/ProfileTab/DonorProfile.dart';
import 'package:uitaskno1/Donor/ProjectTab/ProjectChildren.dart';
import 'package:uitaskno1/Donor/ProjectTab/projbody.dart';
import 'package:uitaskno1/data/services/api_service.dart';
import 'package:uitaskno1/data/services/projects/project_service.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import '../../connector/projects/project_connector.dart';
import 'package:uitaskno1/main.dart' as m;
import 'package:titled_navigation_bar/titled_navigation_bar.dart';
class MyScreen extends StatefulWidget {
  const MyScreen({Key? key}) : super(key: key);

  @override
  State<MyScreen> createState() => _MyScreenState();
}

class _MyScreenState extends State<MyScreen> {
  static final List<Widget> _pages = <Widget>[
    HomeChildren(),
    const ProjBody(),
    const Text('hi'),
    const Text('hi'),
    const DonorProfile()
  ];

  final ScrollController _scrollController = ScrollController();
  int _currentIndex = 0;
  Map<String, dynamic>? apiResponse;

  @override
  Widget build(BuildContext context) {
    return ProjectConnector(
        builder: (BuildContext c, ProjectViewModel projectViewModel) {
      return SafeArea(
        child: Scaffold(
            bottomNavigationBar: Container(

              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(100),

              ),
              child: BottomNavigationBar(
                backgroundColor: Colors.blue,
                selectedItemColor: Colors.deepOrange,
                showUnselectedLabels: true,
                selectedLabelStyle: const TextStyle(color: Colors.deepOrange),
                unselectedItemColor: Colors.grey[500],
                unselectedLabelStyle: const TextStyle(color: Colors.black),
                currentIndex: _currentIndex,
                onTap: (int index) {
                  if (index == 0) {
                    projectViewModel.projectgetAction('planned');
                  }
                  setState(() {
                    _currentIndex = index;
                  });
                },
                items: const [
                  BottomNavigationBarItem(icon: Icon(Icons.home), label: 'home'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.temple_hindu_outlined), label: 'projects'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.currency_rupee, size: 20, weight: 20),
                      label: ''),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.notification_important),
                      label: 'Notifications'),
                  BottomNavigationBarItem(
                      icon: Icon(Icons.person), label: 'profile')
                ],
              ),
            ),
            body: _pages.elementAt(_currentIndex)),
      );
    });
  }
}
