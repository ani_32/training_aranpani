import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:scroll_snap_list/scroll_snap_list.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:uitaskno1/connector/projects/project_connector.dart';
import '../Payments/PaymentHistory.dart';

class HomeChildren extends StatefulWidget {
  @override
  State<HomeChildren> createState() => _HomeChildrenState();
}

List<String?> urls = [];

class _HomeChildrenState extends State<HomeChildren> {
  int _currentIndex = 0;
  int temp = 0;

  @override
  Widget build(BuildContext context) {
    return ProjectConnector(
        builder: (BuildContext context, ProjectViewModel projectViewModel) {
      for (int i = 0; i < projectViewModel.projects.length; i++) {
        urls.add(projectViewModel.projects[i].projectAttachment?[0].image_url);
      }

      if (urls.isNotEmpty) {
        return Stack(
          children: [
            Column(children: [
              Expanded(
                child: Container(
                  decoration: const BoxDecoration(
                      gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.orange, Colors.deepOrange],
                  )),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                ),
              ),
              Stack(
                children: [
                  Container(
                      color: Colors.white,
                      height: MediaQuery.of(context).size.height * 0.6,
                      child: CarouselSlider.builder(
                        options: CarouselOptions(
                          height: double.infinity,
                          enlargeCenterPage: true,
                          pageSnapping: true,
                          viewportFraction: 0.8,
                          onPageChanged: (index, _) {
                            setState(() {
                              _currentIndex = index;
                            });
                          },
                        ),
                        itemCount: projectViewModel.projects.length,
                        itemBuilder:
                            (BuildContext context, int index, int realIndex) {

                          return Stack(
                            children: [
                              Container(
                                width: double.infinity,
                                height: double.infinity,
                                child: Image.network(
                                  urls[index]!,
                                  fit: BoxFit.fitHeight,
                                ),
                              ),
                              Positioned(
                                  top: 10,
                                  left: 20,
                                  child: SizedBox(
                                    child: Container(
                                      width: 30,
                                      height: 40,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white.withOpacity(0.1)),
                                      child: index < 10 && index != 0
                                          ? Text(
                                              '0' '$index',
                                              style: const TextStyle(
                                                  color: Colors.white),
                                            )
                                          : Text(
                                              '${index}',
                                              style: const TextStyle(
                                                  color: Colors.white),
                                            ),
                                    ),
                                  )),
                              Positioned(
                                  top: 400,
                                  left: 20,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      SizedBox(
                                        width: 220,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              projectViewModel.projects[index].name
                                                  .toString(),
                                              style: const TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 14),
                                            ),
                                            const Text(
                                              'Thirumanencheri, Chennai (X4X4 +W6)',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.normal,
                                                  fontSize: 12),
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 30,
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                              width: 30,
                                              height: 30,
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color:
                                                      Colors.grey.withOpacity(0.9)),
                                              child: const Icon(
                                                Icons.notifications_active,
                                                color: Colors.white,
                                                size: 15,
                                              )),
                                        ],
                                      )
                                    ],
                                  )),
                              Positioned(
                                top: 450,
                                left: 20,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        const Text(
                                          'Expensed',
                                          style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          projectViewModel
                                              .projects[index].expensed_amount
                                              .toString(),
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      width: 75,
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          '${projectViewModel.projects[index].completion?.toDouble()} %',
                                          style: const TextStyle(
                                              fontSize: 13, color: Colors.white,fontWeight: FontWeight.w700),
                                        ),
                                        Container(
                                            width: 100,
                                            child: LinearProgressIndicator(
                                              color: Colors.green,
                                              value: projectViewModel
                                                      .projects[index].completion!/100
                                            )),
                                        Text(
                                          'updated on ${projectViewModel.projects[index].start_date?.substring(1, 10)}',
                                          style: const TextStyle(
                                              fontWeight: FontWeight.w700,
                                              color: Colors.white),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              )
                            ],
                          );
                        },
                      )),
                ],
              ),
            ]),
            const Positioned(
                top: -10,
                left: -20,
                child: Image(
                  image: AssetImage('images/mandala.png'),
                )),
            const Positioned(
                top: -70,
                right: -90,
                child: Image(
                  image: AssetImage('images/Component_2_6.png'),
                  width: 260,
                  color: Colors.white54,
                )),
            const Positioned(
              top: 40,
              left: 30,
              child: CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://image.shutterstock.com/image-photo/young-handsome-man-beard-wearing-260nw-1768126784.jpg'),
                radius: 25,
              ),
            ),
            const Positioned(
              top: 40,
              left: 91,
              child: Text(
                'Sivayanama!!',
                style: TextStyle(color: Colors.white),
              ),
            ),
            const Positioned(
                top: 60,
                left: 90,
                child: Text(
                  'Hello, Vijay',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 30),
                )),
            const Positioned(
                top: 110,
                left: 50,
                child: Text(
                  '2020 Donations',
                  style: TextStyle(color: Colors.white),
                )),
            const Positioned(
                top: 130,
                left: 50,
                child: Text(
                  '₹5,400',
                  style: TextStyle(
                      fontSize: 32,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                )),
            Positioned(
                top: 125,
                left: 200,
                child: TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PaymentHistory()));
                    },
                    child: const Text(
                      'Tap to donate',
                      style: TextStyle(color: Colors.white),
                    )))
          ],
        );
      }
      return const SizedBox(
        width: 200,
        height: 200,
        child: CircularProgressIndicator(
          strokeWidth: 3,
        ),
      );
    });
  }
}
