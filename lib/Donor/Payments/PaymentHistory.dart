import 'package:flutter/material.dart';

class PaymentHistory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.3,
              decoration: BoxDecoration(color: Colors.blue[900]),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  const Text(
                    "PAYMENT",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 30),
                  ),
                  ElevatedButton(
                      onPressed: () {},
                      child: const Text(
                        "PAY ₹ 300",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold),
                      )),
                  TextButton(
                      onPressed: () {},
                      child: const Column(
                        children: [
                          Text(
                            "Tap to donate for June 2020",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 19),
                          ),
                          Text(
                            'Donating for 1 (You) + 2 group members',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w200,
                                fontSize: 12),
                          )
                        ],
                      )),
                  const Row(
                    children: [
                      Icon(Icons.info_outline),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '  1st to 10th of a month and after that payment option',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w200,
                                  fontSize: 12),
                            ),
                            Text(
                              '  will be disabled',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w200,
                                  fontSize: 12),
                            )
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            Container(
              color: Colors.grey[900],
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      color: Colors.white
                    ),
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                     children: [ Container(decoration: BoxDecoration(
                     color: Colors.grey,
                       borderRadius: BorderRadius.circular(30)
                     ),child: const Text(' Your donation so far '),),Text("₹ 5,400",style: TextStyle(fontSize: 24,fontWeight: FontWeight.bold),)],
                    ),
                  ),

                  Row(children: [Text('PAYMENT HISTORY'),Text('YEAR 2020')],),
                 Card(
                   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
                 )



                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
