import 'package:flutter/material.dart';
import 'package:uitaskno1/Donor/ProfileTab/dropdowns/AppLangCard.dart';
import 'package:uitaskno1/Donor/ProfileTab/dropdowns/GroupCard.dart';
import 'package:uitaskno1/Donor/ProfileTab/dropdowns/ProfileCard.dart';
import 'package:uitaskno1/Donor/ProfileTab/dropdowns/SubscriptionCard.dart';
import 'package:uitaskno1/views/Navigations.dart';

import 'dropdowns/RepCard.dart';

class DonorProfile extends StatefulWidget {
  const DonorProfile({Key? key}) : super(key: key);

  @override
  State<DonorProfile> createState() => _DonorProfileState();
}

class _DonorProfileState extends State<DonorProfile> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(alignment: Alignment.center, children: [
        Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.1,
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [Colors.orange, Colors.deepOrange])),
            child: Row(
              children: [
                IconButton(
                    onPressed: () {},
                    icon: const Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    )),
                const Text(
                  "Profile",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 24),
                ),
              ],
            ),
          ),
          Container(
              decoration: BoxDecoration(color: Colors.grey[101]),
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.1),
              alignment: Alignment.bottomCenter,
              height: MediaQuery.of(context).size.height * 0.6,
              child: Column(
                children: [
                  const Text(
                    'Vijay',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Expanded(
                    child: ListView(


                        children: [
                      ProfCardList(),
                      RepCardList(),
                      SubCardList(),
                      GroupCardList(),
                      LangList()
                    ]),
                  ),
                ],
              )),
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                color: Colors.grey[100],
                borderRadius: BorderRadius.circular(10)),
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(
                  Icons.lock_open,
                  color: Colors.grey,
                ),
                TextButton(
                    onPressed: () {},
                    child: const Text(
                      "CHANGE PASSWORD",
                      style: TextStyle(color: Colors.grey),
                    ))
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                color: Colors.grey[100],
                borderRadius: BorderRadius.circular(10)),
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(
                  Icons.logout,
                  color: Colors.grey,
                ),
                TextButton(
                    onPressed: () {},
                    child: const Text(
                      "LOGOUT",
                      style: TextStyle(color: Colors.grey),
                    ))
              ],
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Stack(
            children: [
              Positioned(
                  left: 160,
                  top: -8,
                  child: Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.grey.withOpacity(0.3)),
                      child: const Icon(
                        Icons.headset_mic,
                        color: Colors.blue,
                      ))),
              Container(
                width: MediaQuery.of(context).size.width * 0.9,
                height: MediaQuery.of(context).size.height * 0.2,
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                color: Color(int.parse("D6D8E5", radix: 16)),
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.grey[100],
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: const Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Contact Admin',
                            style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(children: [
                            SizedBox(
                              width: 15,
                            ),
                            Icon(
                              Icons.call,
                              color: Colors.grey,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text('+91 98409 98374')
                          ]),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            children: [
                              SizedBox(
                                width: 15,
                              ),
                              Icon(
                                Icons.email,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(' contactapak@gmail.com')
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          )
        ]),
        const Positioned(
          top: 55,
          right: 135,
          child: CircleAvatar(
            backgroundImage: NetworkImage(
                'https://image.shutterstock.com/image-photo/young-handsome-man-beard-wearing-260nw-1768126784.jpg'),
            radius: 55,
          ),
        ),
        Positioned(
            top: 135,
            right: 135,
            child: Container(
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Colors.white.withOpacity(0.8)),
              child: const Icon(
                Icons.camera_alt_rounded,
                color: Colors.black,
                size: 18,
              ),
            ))
      ]),
    );
  }
}
