import 'package:flutter/material.dart';

class GroupCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
              width: MediaQuery.of(context).size.width*0.9,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(90),
                  color: const Color(0xFA8072)),
              child: Container(
               // width: MediaQuery.of(context).size.width*0.4,
                width: 335,
                height: 32,
                //height: MediaQuery.of(context).size.height*0.04,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.deepOrangeAccent.withOpacity(0.1)
                ),
                child: TextButton(
                  onPressed: () {},
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(
                        Icons.group_add,
                        size: 16,

                        color: Colors.deepOrange,
                      ),
                      Text(
                        ' ADD GROUP MEMBER',
                        style: TextStyle(color: Colors.deepOrange),
                      ),
                    ],
                  ),
                ),
              )),
          Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            elevation: 5,
            shadowColor: Colors.black26,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.2,
              width: MediaQuery.of(context).size.width * 0.8,
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(10)),
              child: const Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Priya Bhavani Shankar",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Icon(Icons.more_vert)
                    ],
                  ),
                  Row(
                    children: [
                      Icon(Icons.call),
                      Text(
                        "+91,98797 86786",
                        style: TextStyle(fontWeight: FontWeight.w200),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Icon(Icons.info_outline),
                      SizedBox(width: 10,),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Registration number",
                            style: TextStyle(fontWeight: FontWeight.w200),
                          ),
                          Text(
                            "APAK23909",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class GroupCardList extends StatefulWidget {
  @override
  _GroupCardListState createState() => _GroupCardListState();
}

class _GroupCardListState extends State<GroupCardList> {
  bool visible = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              visible = !visible;
            });
          },
          child: Container(
            padding:
                const EdgeInsets.symmetric(vertical: 12.0, horizontal: 36.0),
            child: const Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.person,
                      color: Colors.grey,
                    ),
                    Text(
                      ' My Group Members',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 111,
                    ),
                    Icon(Icons.arrow_forward_ios)
                  ],
                ),
                Divider()
              ],
            ),
          ),
        ),
        Visibility(
          //visible: cardVisibilityList[index],
          child: AnimatedContainer(
              duration: const Duration(milliseconds: 300),
              height: visible ? 220.0 : 0.0,
              child: GroupCard()),
        ),
        const SizedBox(height: 16.0),
      ],
    );
    //   },
    // );
  }
}
