// import 'package:flutter/material.dart';
//
import 'package:flutter/material.dart';
import 'package:uitaskno1/views/CustomCard.dart';

class RepCardList extends StatefulWidget {
  @override
  _RepCardListState createState() => _RepCardListState();
}

class _RepCardListState extends State<RepCardList> {
  bool visible = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              visible = !visible;
            });
          },
          child: Container(
            padding:
                const EdgeInsets.symmetric(vertical: 12.0, horizontal: 36.0),
            child: const Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.person,
                      color: Colors.grey,
                    ),
                    Text(
                      ' My Representative ',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 111,
                    ),
                    Icon(Icons.arrow_forward_ios)
                  ],
                ),
                Divider()
              ],
            ),
          ),
        ),
        Visibility(
          child: AnimatedContainer(
              duration: const Duration(milliseconds: 300),
              height: visible ? 150.0 : 0.0,
              child: SingleChildScrollView(
                  child: Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: const CustomCard()))),
        ),
        const SizedBox(height: 16.0),
      ],
    );
  }
}
