import 'dart:math';

import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:uitaskno1/main.dart';
import 'package:uitaskno1/l10n/l10n.dart';
class AppLang extends StatefulWidget {
  @override
  State<AppLang> createState() => _AppLangState();
}

class _AppLangState extends State<AppLang> {
  String lang = "english";


  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Row(
            children: [
              Radio(
                value: "English",
                groupValue: lang,
                onChanged: (val) {
                  setState(() {
                    lang = val.toString();
                  });
                  ;
                },
              ),
              const Text("English")
            ],
          ),
          Row(
            children: [
              Radio(
                value: "தமிழ்",
                groupValue: lang,
                onChanged: (val) {
                  setState(() {
                    lang = val.toString();
                  });
                  ;
                },
              ),
              const Text("தமிழ்")
            ],
          ),
          SizedBox(
              width: 400,
              child: ElevatedButton(
                onPressed: () {
                  if(lang!='') {
                    if (lang == 'English') {
                       MyApp.of(context)
                          ?.setLocale(
                          const Locale.fromSubtags(languageCode: 'en'));
                    L10n().setLocale('en');



                    } else {


                       MyApp.of(context)
                          ?.setLocale(
                          const Locale.fromSubtags(languageCode: 'ta'));
                      L10n().setLocale('ta');
                    }
                  }
                },
                style: ElevatedButton.styleFrom(shape: const LinearBorder()),
                child: Text("Continue with $lang"),
              ))
        ],
      ),
    );
  }
}

class LangList extends StatefulWidget {
  @override
  _LangListState createState() => _LangListState();
}

class _LangListState extends State<LangList> {
  bool visible = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              visible = !visible;
            });
          },
          child: Container(
            padding:
                const EdgeInsets.symmetric(vertical: 12.0, horizontal: 36.0),
            child: const Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.language,
                      color: Colors.grey,
                    ),
                    Text(
                      ' Choose App Language',
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 100,
                    ),
                    Icon(Icons.arrow_forward_ios)
                  ],
                ),
                Divider()
              ],
            ),
          ),
        ),
        Visibility(
          child: AnimatedContainer(
            duration: const Duration(milliseconds: 300),
            height: visible ? 250.0 : 0.0,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
              child: AppLang(),
            ),
          ),
        ),
        // const SizedBox(height: 16.0),
      ],
    );
    //   },
    // );
  }
}
