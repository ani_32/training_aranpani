// import 'package:flutter/material.dart';
//
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ProfileCard extends StatelessWidget {
  const ProfileCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 25,
      shadowColor: Colors.black38,
      child: Container(
        //color: Colors.white,

        height: MediaQuery.of(context).size.height * 0.1,
        width: MediaQuery.of(context).size.width * 0.8,
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text(
                  "Vijay Kumar",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Row(
                  children: [
                    Icon(
                      Icons.call,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text("+91 98797 86786")
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                const Row(
                  children: [
                    Icon(
                      Icons.location_pin,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: 9,
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        Text("Akshaya Nagar 1st Block 1st Cross,",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 12)),
                        Text("Rammurthy Nagar Chennai 641666",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 12))
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                const Row(
                  children: [
                    Icon(
                      Icons.supervisor_account_rounded,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        Text('Father/Husband name',
                            style: TextStyle(
                                fontWeight: FontWeight.w300, fontSize: 12)),
                        Text('Moorthy Krishnan',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 12))
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                const Row(
                  children: [
                    Icon(
                      Icons.info,
                      color: Colors.grey,
                      size: 14,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          'Registration Number',
                          style: TextStyle(
                              fontWeight: FontWeight.w200, fontSize: 10),
                        ),
                        Text('APAK23909')
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.62,
                      height: MediaQuery.of(context).size.height * 0.038,
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.1),
                          borderRadius: BorderRadius.circular(20)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.edit,
                            color: Colors.grey,
                          ),
                          TextButton(
                            child: const Text(
                              'EDIT PROFILE DETAILS',
                              style: TextStyle(color: Colors.grey),
                            ),
                            onPressed: () {},
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ProfCardList extends StatefulWidget {
  @override
  _ProfCardListState createState() => _ProfCardListState();
}

class _ProfCardListState extends State<ProfCardList> {
  bool visible = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              visible = !visible;
            });
          },
          child: Container(
            padding:
                const EdgeInsets.symmetric(vertical: 12.0, horizontal: 36.0),
            child: const Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.person,
                      color: Colors.grey,
                    ),
                    Text(
                      'My Profile Details',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 120,
                    ),
                    Icon(Icons.arrow_forward_ios),
                  ],
                ),
                Divider()
              ],
            ),
          ),
        ),
        Visibility(
          //visible: cardVisibilityList[index],
          child: AnimatedContainer(
              duration: const Duration(milliseconds: 300),
              height: visible ? 170.0 : 0.0,
              child: const ProfileCard()),
        ),
        const SizedBox(height: 16.0),
      ],
    );
    //   },
    // );
  }
}
