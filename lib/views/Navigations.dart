import 'package:uitaskno1/Donor/HomeTab/Home.dart';
import 'package:uitaskno1/views/auth/ForgotPassword.dart';
import 'package:uitaskno1/views/auth/LoginScreen.dart';
import 'package:uitaskno1/views/auth/RepresentativeDetails.dart';
import 'package:uitaskno1/views/auth/ResetPassword.dart';
import 'package:uitaskno1/views/auth/SignUp.dart';

import 'package:flutter/material.dart';
import 'package:uitaskno1/views/auth/VerifyOTP.dart';
import 'package:uitaskno1/views/auth/WelcomeOnBoard.dart';

class Navigations
{
  Future gotoLogin(BuildContext context)
  {
    return Navigator.push(context,MaterialPageRoute(builder: (context)=>LoginScreen()) );
  }

  Future gotoSignup(BuildContext context)
  {
    return Navigator.push(context,MaterialPageRoute(builder: (context)=>SignUp()) );
  }

  Future gotoOtp(BuildContext context)
  {
    return Navigator.push(context,MaterialPageRoute(builder: (context)=>VerifyOtp()) );
  }

  Future gotoresetPassword(BuildContext context)
  {
    return Navigator.push(context,MaterialPageRoute(builder: (context)=>ResetPassword()) );
  }


  Future gotoForgotPassword(BuildContext context)
  {
    return Navigator.push(context,MaterialPageRoute(builder: (context)=>ForgotPassword()) );
  }


  Future gotoMyScreen(BuildContext context)
  {
    return Navigator.push(context, MaterialPageRoute(builder: (context)=>MyScreen()));
  }



  Future gotoWelcomeScreen(BuildContext context)
  {
    return Navigator.push(context, MaterialPageRoute(builder: (context)=>WelcomeScreen()));
  }


  Future gotoRepPage(BuildContext context)
  {
    return Navigator.push(context, MaterialPageRoute(builder: (context)=>RepresentativeDetails()));
  }



}