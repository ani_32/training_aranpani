import 'package:flutter/material.dart';

class CustomContainer extends StatelessWidget {
  final String imageUrl;
  final String? text;
  final IconButton? icon;

  const CustomContainer(
      {super.key, required this.imageUrl, this.text, this.icon});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.18,
      child: Stack(
        alignment: AlignmentDirectional.topEnd,
        children: [
          Positioned(
            top: -70,
            right: -80,
            child: Image(
              image: AssetImage(imageUrl),
              height: 250,
            ),
          ),
          if (text != null)
            Positioned(
              bottom: 20,
              left: 16,
              child: Text(
                text!,
                style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
          if (icon != null)
            Positioned(
              top: 16,
              left: 16,
              child: icon!,
            )
        ],
      ),
    );
  }
}
