import 'package:flutter/material.dart';

class CustomCardPayment extends StatefulWidget {
  final String scheme;

  final String details;

  final String amount;

  CustomCardPayment(
      {required this.scheme, required this.details, required this.amount});

  @override
  State<CustomCardPayment> createState() => _CustomCardPaymentState();
}

class _CustomCardPaymentState extends State<CustomCardPayment> {
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    bool selected = true;
    return InkWell(
      onTap: () {
        setState(() {
          isSelected = !isSelected;
        });
      },
      child: Container(
        width: 100,
        height: 95,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(9),
                border: Border.all(
                  color: isSelected ? Colors.deepOrange : Colors.transparent,
                  width: 1.0,
                ),
              ),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              widget.scheme,
                              style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              widget.details,
                              style: const TextStyle(
                                fontSize: 14,
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Text(
                        widget.amount,
                        style: isSelected
                            ? const TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                color: Colors.deepOrange)
                            : const TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                color: Colors.black),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            if (isSelected)
              const Positioned(
                top: 0,
                right: -2,
                child: Center(
                  child: Icon(
                    Icons.check_circle,
                    color: Colors.deepOrange,
                    size: 20,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
