// import 'package:flutter/material.dart';
// import 'package:uitaskno1/views/Projects/Activity.dart';
// import 'package:uitaskno1/views/Projects/DetailBody.dart';
//
// //
// // class Details extends StatefulWidget {
// //   const Details({Key? key}) : super(key: key);
// //
// //   @override
// //   State<Details> createState() => _DetailsState();
// // }
// //
// // class _DetailsState extends State<Details> {
// //   @override
// //   Widget build(BuildContext context) {
// //     return DefaultTabController(
// //       length: 2,
// //       child: Scaffold(
// //         appBar: AppBar(
// //           title: const Text(
// //             "Arul Valla Nathar Temple",
// //             style: TextStyle(color: Colors.black),
// //           ),
// //           backgroundColor: Colors.grey[200],
// //           bottom: TabBar(
// //             indicator: BoxDecoration(
// //                 borderRadius: BorderRadius.circular(50),
// //                 color: Colors.deepOrange),
// //             indicatorColor: Colors.deepOrange,
// //             unselectedLabelColor: Colors.black,
// //             //controller: _tabController,
// //             tabs: const [
// //               Tab(
// //                 text: 'Details',
// //               ),
// //               Tab(
// //                 text: 'Activity',
// //               )
// //             ],
// //           ),
// //         ),
// //         body: const TabBarView(
// //           children: [DetailsBody(), ActivityBody()],
// //         ),
// //       ),
// //     );
// //   }
// // }
//
// class Details extends StatelessWidget {
//   const Details({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return  Scaffold(
//       body: Column(
//         children: [
//           const Image(image: AssetImage('images/temple.png')),
//           Expanded(
//               child: DefaultTabController(
//             length: 2,
//             child: Column(
//               children: [
//                 const TabBar(
//                   tabs: [
//                     Tab(text: 'Details'),
//                     Tab(text: 'Activity'),
//                   ],
//                 ),
//                 Column(
//                   children: [
//                    DetailsBody()
//                   ],
//                 ),
//                 Column(
//                   children: [
//                     Row(
//                       children: [
//                         IconButton(onPressed: (){}, icon: Icon(Icons.arrow_back))
//                       ],
//                     ),
//                     ActivityBody()
//                   ],
//                 )
//               ],
//             ),
//           ))
//         ],
//       ),
//     );
//   }
// }

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:uitaskno1/views/Projects/Activity.dart';
import 'package:uitaskno1/views/Projects/DetailBody.dart';

class Details extends StatefulWidget {
  String image_url;
  String name;
  String status;
  String updated_at;
  String start_date;
  String end_date;
  double estimated_amount;
  double expensed_amount;
  String incharge_name;
  String incharge_mobile_number;
  double completion;
  String reg_number;

  Details(
      {required this.image_url,
      required this.name,
      required this.status,
      required this.updated_at,
      required this.expensed_amount,
      required this.estimated_amount,
      required this.start_date,
      required this.end_date,
      required this.incharge_mobile_number,
      required this.incharge_name,
      required this.completion,
      required this.reg_number});

  @override
  _DetailsState createState() => _DetailsState(
      image_url: image_url,
      name: name,
      status: status,
      updated_at: updated_at,
      estimated_amount: estimated_amount,
      start_date: status,
      end_date: end_date,
      expensed_amount: expensed_amount,
      incharge_mobile_number: incharge_mobile_number,
      incharge_name: incharge_name,
      completion: completion,
      reg_number: reg_number);
}

class _DetailsState extends State<Details> with SingleTickerProviderStateMixin {
  String image_url;
  String name;
  String status;
  String updated_at;
  String start_date;
  String end_date;
  double estimated_amount;
  double expensed_amount;
  String incharge_name;
  String incharge_mobile_number;
  double completion;
  String reg_number;

  _DetailsState(
      {required this.image_url,
      required this.name,
      required this.status,
      required this.updated_at,
      required this.expensed_amount,
      required this.estimated_amount,
      required this.start_date,
      required this.end_date,
      required this.incharge_mobile_number,
      required this.incharge_name,
      required this.completion,
      required this.reg_number});

  late TabController _tabController;
  bool _showImage = true;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _tabController.addListener(() {
      setState(() {
        _showImage = _tabController.index == 0;
      });
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
              child: Visibility(
                  visible: _showImage,
                  child: const Stack(
                    children: [
                      Positioned(
                          top: 10,
                          left: 10,
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.black,
                          )),
                      //Image.network(image_url),
                    ],
                  ))),
          Positioned(
              top: 10,
              left: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                    onPressed: () {


                    },
                    icon: const Icon(Icons.arrow_back),
                    color: Colors.black,
                  ),
                  Text(
                    name,
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ],
              )),
          Column(
            children: [
              Visibility(
                  visible: _showImage,
                  child:  Stack(
                    children: [
                      const Positioned(
                          top: 10,
                          left: 10,
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          )),
                      Image.network(image_url,fit: BoxFit.cover,),
                    ],
                  )),
              const SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 27, left: 16, right: 20),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.grey.withOpacity(0.3),

                  ),
                  child: TabBar(
                    controller: _tabController,
                    indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.deepOrange,
                    ),
                    labelColor: Colors.white,
                    unselectedLabelColor:Colors.black,
                    tabs: const [
                      Tab(text: 'Details'),
                      Tab(text: 'Activity'),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    SingleChildScrollView(
                      child: Column(
                        children: [
                          DetailsBody(
                              name: name,
                              updated_at: updated_at,
                              start_date: start_date,
                              end_date: end_date,
                              expensed_amount: expensed_amount,
                              estimated_amount: estimated_amount,
                              status: status,
                              completion: completion,
                              incharge_name: incharge_name,
                              incharge_mobile_number: incharge_mobile_number,
                              reg_number: reg_number)
                          // Add your text and widgets for the Details tab here
                        ],
                      ),
                    ),
                    const SingleChildScrollView(
                      child: Column(
                        children: [ActivityBody()],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
