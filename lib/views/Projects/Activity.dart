import 'package:flutter/material.dart';
import 'package:readmore/readmore.dart';

class ActivityBody extends StatefulWidget {
  const ActivityBody({Key? key}) : super(key: key);

  @override
  State<ActivityBody> createState() => _ActivityBodyState();
}

class _ActivityBodyState extends State<ActivityBody> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const SizedBox(
            height: 20,
          ),
          const Text(
            '28 Jan 2021 ',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w200),
          ),
          const SizedBox(
            height: 20,
          ),
          Card(
              shadowColor: Colors.black,
              elevation: 0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                child: const Column(children: [
                  ReadMoreText(
                    'Foundation for the temple is done and the construction work is being started. A.. representative closely watches all the progress done in the temple construction',
                    trimCollapsedText: '...Read more',
                    trimExpandedText: ' Read less',
                    trimMode: TrimMode.Line,
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                  Row(
                    children: [
                      Image(
                        image: AssetImage("images/templeimage.png"),
                        width: 50,
                      ),
                      Text(" + 1 more")
                    ],
                  ),
                ]),
              )),
          const SizedBox(
            height: 20,
          ),
          const Text(
            '28 Jan 2021 ',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w200),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
                shadowColor: Colors.black,
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  children: [
                    ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: const Image(
                          image: AssetImage('images/templeimage.png'),
                          fit: BoxFit.cover,
                        )),
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: ReadMoreText(
                        'Foundation for the temple is done and the construction work is being started. All the Area representative closely watches all the progress done in the temple construction',
                        trimCollapsedText: '...Read more',
                        trimExpandedText: ' Read less',
                        trimMode: TrimMode.Line,
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w400),
                      ),
                    )
                  ],
                )),
          ),
          const SizedBox(
            height: 20,
          ),
          Card(
            elevation: 0,
            shadowColor: Colors.black,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
              child: const Column(
                children: [
                  ReadMoreText(
                    'Foundation for the temple is done and the construction work is being started. A.. representative closely watches all the progress done in the temple construction',
                    trimCollapsedText: '...Read more',
                    trimExpandedText: ' Read less',
                    trimMode: TrimMode.Line,
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                  Row(
                    children: [
                      Image(
                        image: AssetImage("images/templeimage.png"),
                        fit: BoxFit.cover,
                        width: 50,
                      ),
                      Text(" + 1 more")
                    ],
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Card(
            elevation: 0,
            shadowColor: Colors.black,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
              child: const Column(
                children: [
                  ReadMoreText(
                    'Foundation for the temple is done and the construction work is being started. A.. representative closely watches all the progress done in the temple construction',
                    trimCollapsedText: '...Read more',
                    trimExpandedText: ' Read less',
                    trimMode: TrimMode.Line,
                    style: TextStyle(fontWeight: FontWeight.w400, fontSize: 15),
                  ),
                  Row(
                    children: [
                      Image(
                        image: AssetImage("images/templeimage.png"),
                        width: 50,
                      ),
                      Text(" + 1 more")
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
