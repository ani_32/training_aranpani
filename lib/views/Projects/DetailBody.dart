import 'package:flutter/material.dart';

class DetailsBody extends StatefulWidget {
  final String name;
  final String status;
  final String updated_at;
  final String start_date;
  final String end_date;
  final double estimated_amount;
  final double expensed_amount;
  final String incharge_name;
  final String incharge_mobile_number;
  final double completion;
  final String reg_number;

  DetailsBody(
      {required this.name,
      required this.status,
      required this.updated_at,
      required this.start_date,
      required this.end_date,
      required this.estimated_amount,
      required this.expensed_amount,
      required this.incharge_mobile_number,
      required this.incharge_name,
      required this.completion,
      required this.reg_number});

  //const DetailsBody({Key? key,r=}) : super(key: key);

  @override
  State<DetailsBody> createState() => _DetailsBodyState(
      name: name,
      status: status,
      updated_at: updated_at,
      estimated_amount: estimated_amount,
      start_date: status,
      end_date: end_date,
      expensed_amount: expensed_amount,
      incharge_name: incharge_name,
      incharge_mobile_number: incharge_mobile_number,
      completion: completion,
      reg_number: reg_number);
}

class _DetailsBodyState extends State<DetailsBody> {
  String name;
  String status;
  String updated_at;
  String start_date;
  String end_date;
  double estimated_amount;
  double expensed_amount;
  String incharge_name;
  String incharge_mobile_number;
  double completion;
  String reg_number;

  _DetailsBodyState(
      {required this.name,
      required this.status,
      required this.updated_at,
      required this.start_date,
      required this.end_date,
      required this.estimated_amount,
      required this.expensed_amount,
      required this.incharge_name,
      required this.incharge_mobile_number,
      required this.completion,
      required this.reg_number});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            name,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              const Icon(
                Icons.circle,
                size: 6,
                color: Colors.green,
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                "$status project ",
                style: const TextStyle(fontSize: 12, color: Colors.green),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              const Text('Registration Number'),
              const SizedBox(
                width: 40,
              ),
              Text(
                reg_number,
                style: const TextStyle(fontWeight: FontWeight.bold),
              )
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          const Row(
            children: [
              Icon(Icons.calendar_month),
              SizedBox(
                width: 10,
              ),
              Text(
                "28 Jan, 2021 - 03 Feb, 2022",
                style: TextStyle(fontWeight: FontWeight.bold),
              )
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              const Icon(
                Icons.call,
                color: Colors.blue,
              ),
              Text(
                '  $incharge_name ',
                style: const TextStyle(color: Colors.blue),
              ),
              Text(
                " $incharge_mobile_number",
                style: const TextStyle(color: Colors.blue),
              )
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Expensed',
                    style: TextStyle(fontWeight: FontWeight.w200),
                  ),
                  Text(
                    "$expensed_amount",
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 24),
                  ),
                  Text(
                    'Estimate: $estimated_amount',
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.bold),
                  )
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "$completion % Completion",
                    style: const TextStyle(fontSize: 13),
                  ),
                  Container(
                      width: 100,
                      color: Colors.grey,
                      child: LinearProgressIndicator(
                        color: Colors.green,
                        value: completion / 100,

                      )),
                  Text(
                    'Updated on ${updated_at.toString().substring(10)}',
                    style: const TextStyle(fontWeight: FontWeight.w200),
                  )
                ],
              )
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Center(
              child: Container(
            width: MediaQuery.of(context).size.width,
            height: 200,
            color: Colors.blue.withOpacity(0.3),
          ))
        ],
      ),
    );
  }
}
