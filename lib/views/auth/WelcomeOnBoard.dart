// import 'package:flutter/material.dart';
// import 'package:uitaskno1/views/CustomCard.dart';
//
// class WelcomeOnboard extends StatelessWidget {
//   const WelcomeOnboard({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//         child: Scaffold(
//       body: Column(
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: [
//           Expanded(
//               child: ListView(
//             shrinkWrap: true,
//             padding: const EdgeInsets.symmetric(horizontal: 15),
//             children: [
//               // const Stack(
//               //  // alignment: AlignmentDirectional.topCenter,
//               //   children: [
//               //     Positioned(
//               //         top: -5,
//               //         child:
//               //             Image(image: AssetImage("images/Component_2_16.png")))
//               //   ],
//               // ),
//               const Image(image: AssetImage("images/namaste.png")),
//               const Text('Sivayanama!!'),
//               const Text("Hello,Vijay Kumar"),
//               const Text("Registration Number   APAK23909"),
//               const SizedBox(
//                 height: 30,
//               ),
//             CustomCard(),
//               const SizedBox(
//                 height: 50,
//               ),
//               ElevatedButton(onPressed: () {}, child: const Text("Start Exploring"))
//             ],
//           ))
//         ],
//       ),
//     ));
//   }
// }
import 'package:flutter/material.dart';
import 'package:uitaskno1/connector/projects/project_connector.dart';
import 'package:uitaskno1/views/CustomCard.dart';
import 'package:uitaskno1/Donor/HomeTab/Home.dart';
import 'package:uitaskno1/views/Navigations.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProjectConnector(
        builder: (BuildContext context, ProjectViewModel projectViewModel) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Scaffold(
              body: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Stack(
                    alignment: AlignmentDirectional.topCenter,
                    children: [
                      SizedBox(height: 124.18, width: 227.6),
                      Positioned(
                          top: -115,
                          child: Image(
                              image: AssetImage("images/Component_2_16.png")))
                    ],
                  ),
                  const Image(
                    image: AssetImage("images/namaste.png"),
                  ),
                  const Text(
                    'Sivayanama!!',
                    style: TextStyle(fontWeight: FontWeight.w400,fontSize: 18),
                  ),
                  const Text(
                    'Hello, Vijaya Kumar!!',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                  ),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Registration number  APAK23909',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 14),
                      ),
                    ],
                  ),
                  const Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Your Representative'),
                        SizedBox(
                          height: 10,
                        ),
                        CustomCard()
                      ],
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.06,
                    child: AbsorbPointer(
                      absorbing: false,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigations().gotoMyScreen(context);
                          projectViewModel.projectgetAction('planned');
                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.deepOrange,
                            shadowColor: Colors.orange,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(1))),
                        child: const Text('START EXPLORING',style: TextStyle(fontSize: 18),),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
