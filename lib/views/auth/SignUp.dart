import 'package:flutter/material.dart';
import 'package:uitaskno1/views/auth/LoginScreen.dart';

import '../Navigations.dart';
import '../TopStack.dart';
import 'about.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          Expanded(
              child: ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(horizontal: 15),
            children: [
              CustomContainer(
                  imageUrl: 'images/Component_2_5.png',
                  text: 'நமச்சிவாய வாழ்க!',
                  icon: IconButton(onPressed: () {}, icon: const Icon(Icons.close))),
              RichText(
                text: const TextSpan(
                  text: '',
                  children: <TextSpan>[
                    TextSpan(
                        text: 'Welcome to',
                        style: TextStyle(fontWeight: FontWeight.w300)),
                    TextSpan(
                        text: 'Aran Pani',
                        style: TextStyle(fontWeight: FontWeight.bold))
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Create Account",
                style: TextStyle(
                    fontSize: 24,
                    fontFamily: 'SF Pro Display',
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 20,
              ),
              Form(
                  child: Column(
                children: [
                  TextFormField(
                    decoration: const InputDecoration(
                      labelText: 'Phone Number',
                      prefixIcon: Icon(Icons.call),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      labelText: 'Password',
                      prefixIcon: Icon(Icons.lock),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                      labelText: 'Confirm Password',
                      prefixIcon: Icon(Icons.lock),
                    ),
                  ),
                ],
              )),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  AbsorbPointer(
                    absorbing: false,
                    child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => About()));
                        },
                        child: const Row(
                          children: [Text('Sign Up'), Icon(Icons.arrow_forward)],
                        )),
                  )
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              const Row(
                children: [
                  Icon(Icons.info_outline),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '  If you are a part of the donation program already, use the same',
                        style: TextStyle(fontSize: 10),
                      ),
                      Text(
                        '  mobile number given to the representative',
                        style: TextStyle(fontSize: 10),
                      ),
                    ],
                  )
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              const Text(
                '    தாங்கள் அரண் பணியில் ஏற்கனவே இணைந்தவராயின்',
                style: TextStyle(fontSize: 11),
              ),
              const Text('    தாங்கள் எமது அமைப்பாளர் இடம் அளித்த அலைபேசி',
                  style: TextStyle(fontSize: 11)),
              const Text('    எண்ணை பயன்படுத்தி பதிவு செய்க',
                  style: TextStyle(fontSize: 11)),
              TextButton(
                  onPressed: (){Navigations().gotoSignup(context);},
                  child: const Text(
                    "Don't have an Account? Sign up here",
                    style: TextStyle(color: Colors.black),
                  ))
            ],
          ))
        ],
      ),
    ));
  }
}
