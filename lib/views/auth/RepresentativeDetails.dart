import 'package:flutter/material.dart';
import 'package:uitaskno1/views/CustomCard.dart';
import 'package:uitaskno1/views/TopStack.dart';
import 'package:uitaskno1/views/auth/NoRepFound.dart';

import 'SignUp.dart';

class RepresentativeDetails extends StatelessWidget {
  const RepresentativeDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          Expanded(
              child: ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(horizontal: 15),
            children: [
              CustomContainer(
                imageUrl: 'images/Component_2_5.png',
                icon: IconButton(onPressed: () {}, icon: const Icon(Icons.close)),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "  Your Representative",
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              const Text(
                  '    Please contact your representative for any questions '),
              const Text('    or concerns '),
              const SizedBox(
                height: 20,
              ),
             CustomCard(),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AbsorbPointer(
                      absorbing: false,
                      child: ElevatedButton(
                          onPressed: () {}, child: const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Proceed'),
                              Icon(Icons.arrow_forward)
                            ],
                          )),
                    ),
                  )
                ],
              ),
            ],
          )),

          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: TextButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => NoRepFound()));
                },
                child: const Text(
                  "Do you have representative reg# ?  Enter here",
                  style: TextStyle(color: Colors.black),
                )),
          )
        ],
      ),
    ));
  }
}
