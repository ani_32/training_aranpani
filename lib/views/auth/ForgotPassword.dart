import 'package:flutter/material.dart';
import 'package:uitaskno1/connector/auth_connector.dart';
import 'package:uitaskno1/views/TopStack.dart';
import 'package:uitaskno1/views/auth/VerifyOTP.dart';

import '../Navigations.dart';
import 'SignUp.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  @override
  State<ForgotPassword> createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final TextEditingController _phnoController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AuthConnector(
        builder: (BuildContext c, AuthViewModel authViewModel) {
      return SafeArea(
          child: Scaffold(
        body: Column(
          children: [
            Expanded(
              child: ListView(
                shrinkWrap: true,
                padding: const EdgeInsets.symmetric(horizontal: 15),
                children: [
                  CustomContainer(
                    imageUrl: 'images/Component_2_6.png',
                    icon: IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.arrow_back),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text(
                    "Forgot Password",
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  const Text('Enter phone number to reset the password'),
                  const SizedBox(
                    height: 30,
                  ),
                  Form(
                      child: Column(
                    children: [
                      TextFormField(
                        controller: _phnoController,
                        decoration: const InputDecoration(
                            labelText: 'Phone Number',
                            prefixIcon: Icon(Icons.call)),
                      )
                    ],
                  )),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      AbsorbPointer(
                        absorbing: false,
                        child: ElevatedButton(
                            onPressed: () {
                              //Navigations().gotoOtp(context);
                              if (true) {
                                authViewModel
                                    .sendOtp(_phnoController.text.trim());
                              }
                            },
                            child: const Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Send OTP'),
                                SizedBox(
                                  width: 30,
                                ),
                                Icon(Icons.arrow_forward)
                              ],
                            )),
                      )
                    ],
                  )
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(bottom: 16),
                child: TextButton(
                    onPressed: () {
                      Navigations().gotoSignup(context);
                    },
                    child: const Text(
                      "Don't have an Account? Sign up here",
                      style: TextStyle(color: Colors.black),
                    )))
          ],
        ),
      ));
    });
  }
}
