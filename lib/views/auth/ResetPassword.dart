import 'package:flutter/material.dart';
import 'package:uitaskno1/views/TopStack.dart';
import 'package:uitaskno1/views/auth/LoginScreen.dart';

import 'SignUp.dart';

class ResetPassword extends StatefulWidget {
  const ResetPassword({Key? key}) : super(key: key);

  @override
  State<ResetPassword> createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          Expanded(
              child: ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(horizontal: 15),
            children: [
              CustomContainer(
                imageUrl: 'images/Component_2_5.png',
                icon: IconButton(
                    onPressed: () {}, icon: const Icon(Icons.arrow_back)),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Reset Password",
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 20,
              ),
              Form(

                  child: Column(
                children: [
                  TextFormField(
                    obscureText: true,
                    decoration: const InputDecoration(
                        labelText: 'New Password',
                        prefixIcon: Icon(Icons.lock),
                        suffixIcon: Icon(Icons.visibility)),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    obscureText: true,
                    decoration: const InputDecoration(
                        labelText: 'Confirm  Password',
                        prefixIcon: Icon(Icons.lock),
                        suffixIcon: Icon(Icons.visibility)),
                  )
                ],
              )),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  AbsorbPointer(
                    absorbing: false,
                    child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const LoginScreen()));
                        },
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [Text('UPDATE'), Icon(Icons.arrow_forward)],
                        )),
                  )
                ],
              )
            ],
          )),
          Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child: TextButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SignUp()));
                },
                child: const Text(
                  "Don't have an Account? Sign up here",
                  style: TextStyle(color: Colors.black),
                )),
          )
        ],
      ),
    ));
  }
}
