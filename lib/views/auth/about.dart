import 'package:flutter/material.dart';
import 'package:uitaskno1/views/Navigations.dart';
import 'package:uitaskno1/views/TopStack.dart';
import 'package:uitaskno1/views/auth/RepresentativeDetails.dart';

class About extends StatefulWidget {
  const About({Key? key}) : super(key: key);

  @override
  State<About> createState() => _AboutState();
}

class _AboutState extends State<About> {
  late final String countryValue;
  late final String stateValue;
  late final String districtValue;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          Expanded(
              child: ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(horizontal: 15),
            children: [
              CustomContainer(
                  imageUrl: 'images/Component_2_5.png',
                  //text: 'நமச்சிவாய வாழ்க!',
                  icon: IconButton(onPressed: () {}, icon: const Icon(Icons.close))),
              RichText(
                text: const TextSpan(
                  text: '',
                  children: <TextSpan>[
                    TextSpan(
                        text: 'Welcome to',
                        style: TextStyle(fontWeight: FontWeight.w300)),
                    TextSpan(
                        text: 'Aran Pani',
                        style: TextStyle(fontWeight: FontWeight.bold))
                  ],
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              const Text(
                "Tell us more about you",
                style: TextStyle(
                    fontSize: 24,
                    fontFamily: 'SF Pro Display',
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 20,
              ),
              Form(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Name'),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                        labelText: 'Name', prefixIcon: Icon(Icons.person)),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text('Email'),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                        labelText: 'Email', prefixIcon: Icon(Icons.email)),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text('Father/Husband Name'),
                  const SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    decoration: const InputDecoration(
                        labelText: 'Father/Husband Name',
                        prefixIcon: Icon(Icons.person)),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Text('Country'),
                  const SizedBox(
                    height: 5,
                  ),
                  DropdownButtonFormField(
                    hint: const Text('Select Country'),
                    items: const [
                      DropdownMenuItem(
                        value: 'Option 1',
                        child: Text('Option 1'),
                      ),
                      DropdownMenuItem(
                        value: 'Option 2',
                        child: Text('Option 2'),
                      ),
                      DropdownMenuItem(
                        value: 'Option 3',
                        child: Text('Option 3'),
                      ),
                    ],
                    onChanged: (value) {
                      setState(() {
                        countryValue = 'option 1';
                      });
                    },
                  ),

                  const SizedBox(
                    height: 10,
                  ),
                  const Text('State'),
                  const SizedBox(
                    height: 5,
                  ),
                  DropdownButtonFormField(
                    hint: const Text('Select State'),

                    items: const [
                      DropdownMenuItem(
                        value: 'Option 1',
                        child: Text('Option 1'),
                      ),
                      DropdownMenuItem(
                        value: 'Option 2',
                        child: Text('Option 2'),
                      ),
                      DropdownMenuItem(
                        value: 'Option 3',
                        child: Text('Option 3'),
                      ),
                    ],
                    onChanged: (value) {
                      setState(() {
                        stateValue = 'option 1';
                      });
                    },
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text('District'),
                  const SizedBox(
                    height: 5,
                  ),
                  DropdownButtonFormField(
                    hint: const Text('Select District'),

                    items: const [
                      DropdownMenuItem(
                        value: 'Option 1',
                        child: Text('Option 1'),
                      ),
                      DropdownMenuItem(
                        value: 'Option 2',
                        child: Text('Option 2'),
                      ),
                      DropdownMenuItem(
                        value: 'Option 3',
                        child: Text('Option 3'),
                      ),
                    ],
                    onChanged: (value) {
                      setState(() {
                        districtValue = 'option 1';
                      });
                    },
                  ),
                  SizedBox(height: 10,),
                  TextFormField(
                    decoration: const InputDecoration(
                      labelText: 'Address',
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      AbsorbPointer(
                        absorbing:false,
                        child: ElevatedButton(
                            onPressed: () {
                              Navigations().gotoRepPage(context);
                            },
                            child: const Row(
                              children: [
                                Text('Proceed'),
                                SizedBox(width: 30,),
                                Icon(Icons.arrow_forward)
                              ],
                            )),
                      )
                    ],
                  ),
                ],
              ))
            ],
          ))
        ],
      ),
    ));
  }
}
