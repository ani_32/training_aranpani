import 'package:flutter/material.dart';
import 'package:uitaskno1/views/TopStack.dart';
import 'package:uitaskno1/views/auth/PaymentScheme.dart';

class NoRepFound extends StatelessWidget {
  const NoRepFound({Key? key}) : super(key: key);

  Widget buildsheet(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SafeArea(
        child: Column(
          //mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Text(
              "Select Representative Manually",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            const SizedBox(
              height: 20,
            ),
            const SizedBox(
                width: 368,
                height: 48,
                child: TextField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Enter representative code',
                  ),
                )),
            const SizedBox(
              height: 215,
            ),
            Expanded(
              flex: 10,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.deepOrange,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(2)
                  )
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const PaymentScheme()),
                  );
                },
                child: const Text('PROCEED'),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomContainer(
            imageUrl: 'images/Component_2_5.png',
            icon: IconButton(
              onPressed: () {},
              icon: const Icon(Icons.close),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          const Padding(
            padding: EdgeInsets.all(7.0),
            child: Text(
              " No representative found",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
            ),
          ),
          const Text(
            "  near your location",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          const SizedBox(
            height: 20,
          ),
          const Text('   Admin will assign a representative. Stay tuned!'),
          const SizedBox(
            height: 20,
          ),
          const Center(
            child: Image(image: AssetImage('images/Group 737.png')),
          ),
          const SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              AbsorbPointer(
                absorbing: false,
                child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const PaymentScheme()));
                    },
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('PROCEED'),
                        SizedBox(
                          width: 30,
                        ),
                        Icon(Icons.arrow_forward)
                      ],
                    )),
              )
            ],
          ),
          const SizedBox(
            height: 150,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 16, left: 30),
            child: TextButton(
                onPressed: () {
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => NoRepFound()));

                  showModalBottomSheet(
                      showDragHandle: true,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      context: context,
                      builder: (context) => buildsheet(context));
                },
                child: const Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Text(
                    "Do you have representative reg# ?  Enter here",
                    style: TextStyle(color: Colors.black),
                  ),
                )),
          )
        ],
      ),
    ));
  }
}
