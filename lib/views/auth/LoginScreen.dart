import 'package:flutter/material.dart';
import 'package:uitaskno1/views/TopStack.dart';
import 'package:uitaskno1/views/auth/ForgotPassword.dart';
import 'package:uitaskno1/views/auth/SignUp.dart';
import 'package:uitaskno1/views/Navigations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:uitaskno1/actions/auth/auth_action.dart';
import 'package:uitaskno1/connector/auth_connector.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late bool ap = false;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _phnoController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AuthConnector(
        builder: (BuildContext c, AuthViewModel authViewModel) {
      return SafeArea(
          child: Scaffold(
        body: Column(
          children: [
            Expanded(
                child: ListView(
              shrinkWrap: true,
              padding: const EdgeInsets.symmetric(horizontal: 15),
              children: [
                const CustomContainer(
                  imageUrl: 'images/Component_2_5.png',
                  text: 'நமச்சிவாய வாழ்க!',
                ),
                // RichText(
                //   text: const TextSpan(
                //     text: '',
                //     children: <TextSpan>[
                //       TextSpan(
                //           text: 'Welcome to',
                //           style: TextStyle(
                //               fontWeight: FontWeight.w300, fontSize: 14)),
                //       TextSpan(
                //           text: 'Aran Pani',
                //           style: TextStyle(
                //               fontWeight: FontWeight.bold, fontSize: 14))
                //     ],
                //   ),
                // ),

                const Text(
                  "Welcome to Aranpani",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  AppLocalizations.of(context)!.logintoyouraccount,
                  style: const TextStyle(
                      fontSize: 24,
                      fontFamily: 'SF Pro Display',
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 30,
                ),
                Form(
                  key: _formKey,
                    child: Column(
                  children: [
                    TextFormField(
                      controller: _phnoController,
                      validator: (value) {
                        if (value.toString().length < 10) {
                          return 'phone number invalid';
                        }
                        return null;
                      },
                      decoration: const InputDecoration(
                        labelText: 'Phone Number',
                        prefixIcon: Icon(Icons.call),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: _passwordController,
                      obscureText: true,
                      decoration: const InputDecoration(
                        labelText: 'Password',
                        prefixIcon: Icon(Icons.lock),
                      ),
                    ),
                  ],
                )),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigations().gotoForgotPassword(context);
                        },
                        child: const Text(
                          'Forgot Password?',
                          style: TextStyle(color: Colors.black),
                        ))
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    AbsorbPointer(
                      absorbing: false,
                      child: ElevatedButton(
                          onPressed: () {
                            print(_formKey.currentState);

                            const SizedBox(
                                width: 200,
                                height: 200,
                                child: CircularProgressIndicator(
                                  strokeWidth: 3,
                                ));

                            setState(() {
                             // ap = !ap;
                            });
                            if (_formKey.currentState!.validate()) {
                              authViewModel.loginWithPassword(
                                  _phnoController.text.trim(),
                                  _passwordController.text.trim());
                              Navigations().gotoMyScreen(context);
                            }
                          },
                          child: Row(
                            children: [
                              Text(
                                AppLocalizations.of(context)!.login,
                              ),
                              const SizedBox(
                                width: 30,
                              ),
                              const Icon(Icons.arrow_forward)
                            ],
                          )),
                    )
                  ],
                ),
              ],
            )),
            TextButton(
                onPressed: () {
                  Navigations().gotoSignup(context);
                },
                child: const Text(
                  "Don't have an Account? Sign up here",
                  style: TextStyle(color: Colors.black),
                ))
          ],
        ),
      ));
    });
  }
}
