import 'package:flutter/material.dart';
import 'package:uitaskno1/views/TopStack.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:uitaskno1/views/auth/ResetPassword.dart';

import '../Navigations.dart';
import 'SignUp.dart';

class VerifyOtp extends StatefulWidget {
  const VerifyOtp({Key? key}) : super(key: key);

  @override
  State<VerifyOtp> createState() => _VerifyOtpState();
}

class _VerifyOtpState extends State<VerifyOtp> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          Expanded(
              child: ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(horizontal: 15),
            children: [
              CustomContainer(
                imageUrl: 'images/Component_2_6.png',
                icon: IconButton(
                  onPressed: () {},
                  icon: Icon(Icons.arrow_back),
                ),
              ),
              const Text(
                "Verify OTP",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
              ),
              const Text('Enter OTP sent to 98434 79900'),
              const SizedBox(
                height: 30,
              ),
              OtpTextField(
                numberOfFields: 4,
                showFieldAsBox: true,
              ),
              Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                Column(
                  children: [
                    TextButton(
                        onPressed: () {},
                        child: const Text(
                          'Resend OTP',
                          style: TextStyle(color: Colors.black),
                        )),
                    const SizedBox(
                      height: 30,
                    ),
                    AbsorbPointer(
                      absorbing: false,
                      child: ElevatedButton(
                          onPressed: (){Navigations().gotoresetPassword(context);},
                          child: const Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [Text('VERIFY'),SizedBox(width: 30,), Icon(Icons.arrow_forward)],
                          )),
                    )
                  ],
                )
              ])
            ],
          )),
          Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child:  TextButton(
                onPressed: (){Navigations().gotoSignup(context);},
                child: const Text(
                  "Don't have an Account? Sign up here",
                  style: TextStyle(color: Colors.black),
                ))
          )
        ],


      ),
    ));
  }
}
