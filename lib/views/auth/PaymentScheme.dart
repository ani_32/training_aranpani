import 'package:flutter/material.dart';
import 'package:uitaskno1/views/CustomCardPayment.dart';
import 'package:uitaskno1/views/Navigations.dart';
import 'package:uitaskno1/views/TopStack.dart';
import 'package:uitaskno1/views/auth/WelcomeOnBoard.dart';

class PaymentScheme extends StatelessWidget {
  const PaymentScheme({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          Expanded(
              child: ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(horizontal: 13),
            children: [
              CustomContainer(
                imageUrl: 'images/Component_2_6.png',
                icon: IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.close),
                ),
              ),
              const Text(
                "Select Payment Scheme",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
              ),
              const SizedBox(
                height: 30,
              ),
              CustomCardPayment(
                  scheme: "Monthly Scheme",
                  details: "Donate 100 for every month",
                  amount: "₹ 100"),
              CustomCardPayment(
                  scheme: "Half Yearly Scheme",
                  details: "Donate 600 for every 6 months",
                  amount: "₹ 600"),
              CustomCardPayment(
                  scheme: "Annual Scheme",
                  details: "Donate 1200 for every year",
                  amount: "₹ 1200"),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  AbsorbPointer(
                    absorbing: false,
                    child: ElevatedButton(
                        onPressed: () {
                          Navigations().gotoWelcomeScreen(context);
                        },
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Confirm'),
                            SizedBox(
                              width: 30,
                            ),
                            Icon(Icons.arrow_forward)
                          ],
                        )),
                  )
                ],
              ),
              const SizedBox(
                height: 40,
              ),
              const Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Icon(Icons.info_outline),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Payments will be multiplied into 2x, 3x based on the number',
                        style: TextStyle(fontSize: 12),
                      ),
                      Text('of group members added',
                          style: TextStyle(fontSize: 12)),
                    ],
                  )
                ],
              )
            ],
          )),
        ],
      ),
    ));
  }
}
