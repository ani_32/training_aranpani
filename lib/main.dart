import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:uitaskno1/l10n/l10n.dart';
import 'package:uitaskno1/reducers/reducers.dart';
import 'package:uitaskno1/views/auth/LoginScreen.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uitaskno1/l10n/l10n.dart';

import 'actions/auth/auth_action.dart';
import 'data/api/api_routes.dart';
import 'data/app_repository.dart';
import 'data/preference_client.dart';
import 'middleware/middleware.dart';
import 'models/app_state.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final SharedPreferences prefs = await SharedPreferences.getInstance();

  final AppRepository repository = AppRepository(
      preferencesClient: PreferencesClient(prefs: prefs),
      config: ApiRoutes.nextCallConfig);
  runApp(MyApp(
    repository: repository,
  ));
}

class MyApp extends StatefulWidget {
  MyApp({super.key, required AppRepository repository})
      : store = Store<AppState>(
          reducer,
          middleware: middleware(repository),
          initialState: AppState.initState(),
        );

  final Store<AppState> store;

  static _MyAppState? of(BuildContext context) =>
      context.findAncestorStateOfType<_MyAppState>();

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale? _locale;
  late Store<AppState> store;

  @override
  void didChangeDependencies() {
    L10n().getLocale().then((locale) {
      setState(() {
        this._locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  void initState() {
    super.initState();
    store = widget.store;
    _init();
  }

  void _init() {
    Future<void>.delayed(const Duration(seconds: 2), () {
      store.dispatch(CheckForUserInPrefs());
    });
  }

  final ButtonStyle elevatedButtonStyle = ElevatedButton.styleFrom(
      backgroundColor: Colors.deepOrange, // Button color
      elevation: 5, // Elevation
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(100), // Button shape
      ),
      minimumSize: const Size(167, 55) // Set the width and height

      );

  final InputDecorationTheme textFormFieldDecoration = InputDecorationTheme(
    
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10)
    ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
      ));

  final ButtonStyle textbuttonstyle = TextButton.styleFrom(
      textStyle: const TextStyle(fontSize: 14, color: Colors.black));

  final ButtonStyle dropdownButtonStyle = ButtonStyle(
    foregroundColor: MaterialStateProperty.all<Color>(Colors.blue),
    // Text color
    padding: MaterialStateProperty.all<EdgeInsets>(const EdgeInsets.all(16)),
    // Button padding
    backgroundColor: MaterialStateProperty.all<Color>(Colors.grey),
    // Button color
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8), // Button shape
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
        store: store,
        child: SafeArea(
          child: MaterialApp(
            locale: _locale,
            localizationsDelegates: const [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate
            ],
            supportedLocales: L10n.all,
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              elevatedButtonTheme:
                  ElevatedButtonThemeData(style: elevatedButtonStyle),
              inputDecorationTheme: textFormFieldDecoration,
              textButtonTheme: TextButtonThemeData(style: textbuttonstyle),
            ),
            home: const LoginScreen(),
          ),
        ));
  }
}
