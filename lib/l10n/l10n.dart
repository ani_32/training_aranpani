
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uitaskno1/main.dart';
class L10n {
  static final all = [
    const Locale('en'),
    const Locale('ta')
  ];


  static const String ENGLISH='en';
  static const String TAMIL='ta';

  static const String LANGUAGE_CODE='languageCode';


  Future<Locale> setLocale(String languagecode) async{
    SharedPreferences _prefs=await SharedPreferences.getInstance();
    await _prefs.setString(LANGUAGE_CODE, languagecode);
    return _locale(languagecode);
  }






  Locale _locale(String languageCode)
  {
    Locale _temp;

    switch(languageCode){
      case ENGLISH:
        _temp=Locale(languageCode);
        break;

      case TAMIL:
        _temp=Locale(languageCode);
        break;


      default:
        _temp=Locale(ENGLISH);


    }
    return _temp;
  }



  Future<Locale> getLocale() async{
    SharedPreferences _prefs=await SharedPreferences.getInstance();
    String languageCode= _prefs.getString(LANGUAGE_CODE)??'en';
    return _locale(languageCode);
  }



}